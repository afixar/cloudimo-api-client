<?php

class HtmlGoogleMaps
{
	
	public static function imprimir($rua=null,$bairro=null,$cidade=null,$uf=null,$cep=null,$width='100%',$height='450px')
	{
		$url_script_google = 'http://afxws.cloudimo.com.br/service/google-maps.js';
		
		//null,'St. Sul','Goiânia','GO','74175-120'
		$end = "'{$rua}','{$bairro}','{$cidade}','{$uf}','{$cep}'";
		
		echo <<<html
			
			<div id="mapGoogle" style="width:{$width};height:{$height};"></div>
			
			<!--script type="text/javascript" src="jquery-2.2.0.min.js"></script-->
				<script type="text/javascript">				
					var url_geocode = getGeocodeUrl({$end});
					function getGeocodeUrl(rua,bair,cid,uf,cep) {
						var address = '';
						if (rua) address += ', '+rua;
						if (bair) address += ', '+bair;
						if (cid) address += ', '+cid;
						if (uf) address += '- '+uf;
						if (cep) address += ', '+cep;
						//'http://maps.google.com/maps/api/geocode/json?address=St.+Sul,+Goi�nia+-+GO,+74175-120';			
						address = address.replace(/\s/ig,'+');
						address = address.replace(/^,\+/ig,'');			
						return 'http://maps.google.com/maps/api/geocode/json?address='+address;
					}
					var mapGoogle;
					function initMap() {		
						$.getJSON(url_geocode, function(data){		
							try {
								geo_location = data.results[0].geometry.location;					
								var myLatLng = geo_location;//{lat: -16.692544, lng: -49.247806};
								var mapGoogle = new google.maps.Map(document.getElementById('mapGoogle'), {
								  zoom: 16,
								  center: myLatLng
								});
								var marker = new google.maps.Marker({
								  position: myLatLng,
								  map: mapGoogle,
								  //title: 'Hello World!'
								});
							}catch(err){}				
						}).fail(function(){});
					}		
				</script>
				<script src="{$url_script_google}" async defer></script>
		
html;
	}
	
}