<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>XML - Error</title>
    </head>
    <body style="background-color:#f4f9fa">
        <div style="text-align:center">
            <div style="border:1px solid #e2e2e2;width:750px;margin:50px auto">
                <div style="height:100px;background-color:#fff;padding-top:50px;background:url(<?php echo get_template_directory_uri(); ?>/images/error_page.gif) top repeat-x">
                    <h1 style="font: bold 150% Arial, Helvetica, sans-serif; color:#5278af;margin:0;padding:0">XML - Error</h1>
                    <h2 style="font: bold 80% Tahoma, Verdana, sans-serif; color:#5278af;margin:0;padding:0">Ops! Não foi possível carregar dados de imóveis.</h2>
                </div>
                <div style="height:15px;background-color:#eef6f7"></div>
            </div>
        </div>
    </body>
</html>