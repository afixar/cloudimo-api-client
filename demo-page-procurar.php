<?php


if (!isset($_GET['pag'])) {
    unset($_SESSION['post_pesquisa']);
}


if (isset ($_POST['act'])) {
    $_SESSION['post_pesquisa'] = $_POST;
    $_GET['pag'] = 1; //iniciar da primeira página
}


if (!isset($post_request)) {
    $post_request = isset ($_SESSION['post_pesquisa']) ? $_SESSION['post_pesquisa'] : null;
}


// Paginação indice = (P-1)*Tp
$limite_pag = $posts_per_page; //10
$atual_pag  = ! isset ($_GET['pag']) ? 1 : $_GET['pag'];
$inicio_pag = ($atual_pag <= 1) ? 1 : $atual_pag;
$inicio_pag = ($inicio_pag -1) * $limite_pag;


$imoveis = null;//preechido por busca

cloudimo_buscar_geral($imoveis, $post_request, array(
    'pagination_start' => $inicio_pag, //0
    'pagination_total' => $limite_pag
));


// Capturando usuarios
$result_usuarios_xml = '';


// Para recuperar dados de post dentro de get_header()
$GLOBALS['post_request'] = $post_request;



// Para mensagem total encontrados.
$total_imoveis = $imoveis->Imoveis->Total;

if ($total_imoveis > 1) {
    $total_msg = 'Encontrado <b>'.$total_imoveis.'</b> Imóveis.';
} else {
    $total_msg = 'Encontrado <b>'.$total_imoveis.'</b> Imóvel.';
}


// Total páginas
$total_pag  = ceil($total_imoveis / $limite_pag);
$id_home = get_id_by_slug('home');

?>


<?php if(empty($titulo)) { echo 'Busca no site'; } else { echo $titulo; } ?>


<?php
// Ver o total do resultado
echo 'Total de resultados ' . $total_msg; ?>

                        
<?php
//Exibir todos os imoveis encontrados
foreach ($imoveis->Imoveis->Imovel as $key => $imovel): ?>

<div class="listing-item">
        <?php 
            $link_imovel = get_page_link(8).'?id='.$imovel->ID;
            if ($imovel->permalink)
                $link_imovel = $imovel->permalink;
        ?>
        <a href="<?=$link_imovel?>" class="listing-img-container">

            <div class="listing-badges">
                <?php $tarja = (string) $imovel->Tarja; if($tarja): ?>
                <span class="featured"><?=$tarja?></span>
                <?php endif; ?>
                <span><?=$imovel->CategoriaImovel?></span>
            </div>

            <div class="listing-img-content">                                                    

                <?php if ($imovel->CategoriaImovel == 'Venda/Locação'): ?>
                <span class="listing-price">Venda R$ <?=$imovel->PrecoVenda?></i></span>
                <span class="listing-price">Locação R$ <?=$imovel->PrecoLocacao?></i></span>
                <?php endif; ?>

                <?php if ($imovel->CategoriaImovel == 'Locação'): ?>
                <span class="listing-price">R$ <?=$imovel->PrecoLocacao?></i></span>
                <?php endif; ?>

                <?php if ($imovel->CategoriaImovel == 'Venda'): ?>
                <span class="listing-price">R$ <?=$imovel->PrecoVenda?></i></span>
                <?php endif; ?>

            </div>

            <img src="<?=cloudimo_get_image($imovel->Fotos->Foto->URLArquivo)?>" alt="">

        </a>

        <div class="listing-content">

            <div class="listing-title">                                                
                <?php if ($imovel->CodigoImovel): ?>
                    <span class="codigo">Código: <?php echo $imovel->CodigoImovel; ?></span>
                <?php endif; ?>
                <h4><a href="<?=$link_imovel?>"><?php echo $imovel->TipoImovel; ?></a></h4>                                               
                <i class="fa fa-map-marker"></i>
                <?php echo $imovel->Bairro . ' - ' . $imovel->Cidade; ?> <?php if($imovel->Uf) echo ' - '.$imovel->Uf; ?>
                <br/>
                <br/>
                <?php if ($imovel->resumo)
                    echo resumir($imovel->resumo, 160, true);
                ?>                                            
                <a href="<?=$link_imovel?>" class="details button border">Mais detalhes</a>
            </div>
            <?php if (!$imovel->resumo): ?>
            <ul class="listing-details">
                <div class="row">
                    <div class="col-md-6">												
                        <?php if ((int) $imovel->QtdSalas) echo "<li><i class='fa fa-home'></i>  {$imovel->QtdSalas} Sala(s)</li>";?>
                        <?php if ((int) $imovel->QtdSuites) echo "<li><i class='fa fa-bed'></i>  {$imovel->QtdSuites} Suíte(s)</li>";?>
                        <?php if ((int) $imovel->QtdDormitorios) echo "<li><i class='fa fa-bed'></i>  {$imovel->QtdDormitorios} Quarto(s)</li>";?>
                    </div>
                    <div class="col-md-6">
                        <?php if ((int) $imovel->QtdBanheiros) echo "<li><i class='fa fa-bath'></i>  {$imovel->QtdBanheiros} Banheiro(s)</li>";?>
                        <?php if ((int) $imovel->QtdVagasGaragem) echo "<li><i class='fa fa-car'></i>  {$imovel->QtdVagasGaragem} Vaga(s)</li>";?>
                    </div>												
                </div>
            </ul>
            <?php endif; //fim resumo ?>
        </div>

</div>
    
<?php endforeach; ?>

<!-- Pagination -->
<?php echo cloudimo_html_pagination($atual_pag, $total_pag); ?>
