# Cloudimo API Client


## Introdução

Acessar dados do sistema Cloudimo, dados como Tipo de imóveis, Imóveis e Usuários.


Documentação da API em [app.cloudimo.com.br/docs/api/](http://app.cloudimo.com.br/docs/api/)


## Como usar

- Copiar esta pasta para a pasta root do seu tema em ``wp-content/themes``

- Inserir em ``functions.php``

```php

    require_once ('cloudimo-api-client/client.php');

    
```

- Configurar no menu ``Cloudimo`` a chave do cliente (imobiliária)


- Exemplo de pesquisa

```php

    // Pesquisar imóveis
    $imoveis = '';
    $_post = array(
        'act'           => 'pesquisar',
        'categoria' 	=> 'locacao',
        'site_destaque' => 1
        //Outras condições aqui
    );
    cloudimo_buscar_imoveis($imoveis, $_post);


    // Pesquisar imóveis e lançamentos
    $imoveis = '';
    $_post = array(
        'act'           => 'pesquisar',
        'categoria' 	=> 'locacao',
        'site_destaque' => 1
        //Outras condições aqui
    );
    $_conditional = array(
        'pagination_start' => 10,
        'pagination_total' => 22
    );
    cloudimo_buscar_geral($imoveis, $_post, $_conditional);

    
```

Ver exemplo [demo-page-procurar.php](demo-page-procurar.php)

- Exemplo de encontrar um imóvel

```php

    $id_imovel = 999;
    $imovel = '';        
    cloudimo_verify_xml( cloudimo_get_xml('imoveis'). '&' . 'id='. $id_imovel , $imovel );
    $imovel = $imovel->Imoveis->Imovel;

    //Código do imóvel
    echo $imovel->CodigoImovel;


```

- Exemplo de exibir Google Maps

```php

    HtmlGoogleMaps::imprimir($imovel->Rua, $imovel->Bairro, $imovel->Cidade, $imovel->Uf);


```

- Exemplo de exibir Vídeo do Imóvel

```php

    if ((string) $imovel->Videos->Video)
        //https://youtu.be/aeZ4xSTFSOw
	    HtmlVideo::imprimir($imovel->Videos->Video);
    }


```


## Arquivos


- ``client.php``

Arquivo principal chamado em fuctions.php do WordPress.
A parti deste arquivos serão chamados outras funções Cliente Cloudimo.


- ``buscar_geral.php``

Função para montar URL de pesquisa na API do Cloudimo.


- ``page.php``

Arquivo responsável por fornecer um formulário para configurar em
modo de interface usuário, chave e url de acesso ao API XML do Cloudimo.


- ``config.php``

Arquivo reponsável pelas principais configurações do Cloudimo API Client.
Como menu e campos para digitar a chave de autorização.


- ``pagination.php``

Arquivo com funções de paginação de pesquisa de imóveis.


- ``session_xml.php``

Arquivo responsável por criar e gerenciar sessões com dados de pesquisa
a API do Cloudimo.
    

- ``verify_xml.php``

Arquivo com funções responsáveis por validar XML de retorno
da API Cloudimo.


- ``error-xml.php``

Página com mensagem de ERRO do XML.
Acontece quando ocorrer algum error de resposta.