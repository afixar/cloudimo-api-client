<?php

/**
 * Exemplo de post_request:
 * 
 *  $post_request = [
 *      'act'       =>'pesquisar',
 *      'categoria' =>'locacao',
 *  ];
 *  cloudimo_buscar_imoveis($imoveis, $post_request, [
        'pagination_start' => $inicio_pag, //0
        'pagination_total' => $limite_pag
    ]);
 * 
 * 
 * @param mixed $result (Variável a receder xml)
 * @param array $post_request (null) Post personalizado
 * @param array $add_post (null) Dadicionar post
 */
function cloudimo_buscar_imoveis(&$result = null, $post_request = null, $add_post = null)
{

    //retorna todos imóveis
    if (!$post_request || !isset ($post_request['act'])) {
        cloudimo_verify_xml( cloudimo_get_xml('imoveis') , $result );
        return false;
    }
    
    //echo '<pre>post';print_r($post); exit;
    $search = buscar_imoveis_params($post_request);          

    if ($add_post) {
        $search = array_merge($search, $add_post);
    }


    $search = http_build_query($search);
    //exit('URL Search: '.cloudimo_get_xml('imoveis'). '&' . $search);


    cloudimo_verify_xml( cloudimo_get_xml('imoveis'). '&' . $search , $result );
    
    /*
    //Imprimir log
    $fopen = fopen('wp-content/themes/cloudimo_quatre/log.txt', "a+");
    $content = PHP_EOL;
    $content .= 'total geral: '.$result->Imoveis->Total;
    $content .=PHP_EOL.'url: '.cloudimo_get_xml('imoveis'). '&' . $search;
    $content.=PHP_EOL.'';
    foreach ($result->Imoveis->Imovel as $val) {
        $content.= $val->CategoriaImovel.PHP_EOL;
    }
    fwrite($fopen,$content);
    fclose($fopen);*/
    

}


function buscar_imoveis_params($post)
{
    
    return array(
        'id_tipo_imovel'    => isset ($post['tipo_imovel'])      ? $post['tipo_imovel']      : null,
        'categoria'         => isset ($post['categoria'])        ? $post['categoria']        : null,
        'procurar'          => isset ($post['procurar'])         ? $post['procurar']         : null,
        'area_util_de'      => isset ($post['area_util_de'])     ? $post['area_util_de']     : null,
        'area_util_ate'     => isset ($post['area_util_ate'])    ? $post['area_util_ate']    : null,
        'valor_de'          => isset ($post['valor_de'])         ? $post['valor_de']         : null,
        'valor_ate'         => isset ($post['valor_ate'])        ? $post['valor_ate']        : null,
        'garagem'           => isset ($post['garagem'])          ? $post['garagem']          : null,
        'salas'             => isset ($post['salas'])            ? $post['salas']            : null,
        'quartos'           => isset ($post['quartos'])          ? $post['quartos']          : null,
        'banheiros'         => isset ($post['banheiros'])        ? $post['banheiros']        : null,
        'site_destaque'     => isset ($post['site_destaque'])    ? $post['site_destaque']    : null,
    );
    
}
