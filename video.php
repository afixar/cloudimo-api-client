<?php

class VerificarVideo
{	
	public function youtube($url)
	{
		$match = '/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=)?)((\w|-){11})(?:\S+)?$/';
		if (preg_match($match, $url)){
			return true;
		}
	}
	
	public function vimeo($url)
	{
		$match = '/^(?:https?:\/\/)?(?:www\.)?(?:vimeo\.com\/|player\.vimeo\.com\/(?:video\/)?)[\d]+$/';		
		if (preg_match($match, $url)){
			return true;
		}		
	}	
}


class HtmlVideo
{	

	public static function imprimir($lnk, $conf=array('W'=>'100%', 'H'=>'450'))
	{		
		$verificarVideo = new VerificarVideo();
		
		if ($verificarVideo->youtube($lnk)) {
			self::imprimirYoutube($lnk, $conf);
		}
		
		if ($verificarVideo->vimeo($lnk)) {
			self::imprimirVimeo($lnk, $conf);
		}
	}

	public static function imprimirYoutube($lnk, $conf)
	{
		//https://www.youtube.com/embed/OsyaD6eJBNY
		$ply = 'https://www.youtube.com/embed/';
		$patt = '/https?:\/\/(www\.)?(youtu\.be\/|youtube\.com\/)(embed\/|v\/|watch\?v=)?([\w_-]+)/';
		$lnk = preg_replace($patt, $ply.'$4', $lnk);
		
		echo '<iframe width="'.$conf['W'].'" height="'.$conf['H'].'" src="'.$lnk.'" frameborder="0" allowfullscreen></iframe>';
	}
	
	public static function imprimirVimeo($lnk, $conf)
	{
		$ply = 'https://player.vimeo.com/video/';		
		$lnk = preg_replace('@https?:\/\/vimeo\.com\/([\d]+)@',$ply.'$1',$lnk);
		
		echo '<iframe src="'.$lnk.'" width="'.$conf['W'].'" height="'.$conf['H'].'"'
			.'frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
	}
}

//Teste
//HtmlVideo::imprimir('https://youtu.be/aeZ4xSTFSOw');
//HtmlVideo::imprimir('https://vimeo.com/197443255');

