<?php

/**
 * Pesquisar em WordPress e Cloudimo Sistema
 * 
 * @param mixed $result (Variável a receder xml)
 * @param array $post_request (null) Post personalizado
 * @param array $add_post (null) Dadicionar post
 */
function cloudimo_buscar_geral(&$result = null, $post_request = null, $add_post = null)
{
 
    //Array de busca
    //[act] => pesquisar
    //[tipo_imovel] => lancamento
    //[categoria] => todos
    //[procurar] =>
    
    $result = new \ArrayObject();
    $result->Imoveis = new \ArrayObject();
    $result->Imoveis->Imovel = new \ArrayObject();
    $result->Imoveis->Total = 0;
    
    
    //retorna todos imóveis
    if (!$post_request || !isset ($post_request['act'])) {
		$post_request['act'] = 'pesquisar';
		$post_request['categoria'] = 'todos';
    }
    
    
    $categoria = isset($post_request['categoria'])? $post_request['categoria']: 'todos';
    $tipo_imovel = isset($post_request['tipo_imovel'])? $post_request['tipo_imovel']: '';
    

	
	//Ao escolher categoria de locar ou comprar ou lançamentos
    if ($categoria == 'venda' || $categoria == 'locacao'){
        cloudimo_buscar_imoveis($result, $post_request, $add_post);
        return null;
    }
	
	
    //Ao escolher todos tipos
    if ($tipo_imovel == '' && ($categoria == 'lancamento' || $categoria=='todos')) {
        cloudimo_buscar_lancamentos($result, $post_request);
    }	
	
	
    $search = buscar_imoveis_params($post_request);
        
    if ($add_post) {
        $search = array_merge($search, $add_post);
    }

    
    $search = http_build_query($search);
    //Teste URL para pesquisa XML
	//exit('URL Search: '.cloudimo_get_xml('imoveis'). '&' . $search);
    
    
    $result_api = null;
    cloudimo_verify_xml( cloudimo_get_xml('imoveis'). '&' . $search , $result_api);

	
	
	if (!$result_api->Imoveis->Imovel[0]){
		return null;
	}
  
  
	
    //Unindo resultado do Cloudimo com WordPress
    foreach ($result_api->Imoveis->Imovel as $val) {
        $result->Imoveis->Imovel[] = $val;
    }
	
	
	//Somando total geral
	$result->Imoveis->Total += $result_api->Imoveis->Total;
	
	
	//echo '<pre>';
	//print_r($result);	
	//exit('result api');
    
}

