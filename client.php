<?php

/**
 * Arquivo responsável por integrar todos arquivos
 * para funcionalidades das requisições
 * ao Sistema Cloudimo.
 */


// Includes e requires
require_once('config.php');
require_once('get_xml.php');
require_once('page.php');
require_once('verify_xml.php');
require_once('session_xml.php');
require_once('buscar_imoveis.php');
require_once('buscar_lancamentos.php');
require_once('buscar_geral.php');
require_once('pagination.php');
require_once('video.php');
require_once('google_maps.php');


/**
 * @param string $path 
 * @param bool $thumb
 */
function cloudimo_get_image($path = null, $thumb = true)
{    
    
    if ($path) {
                
        if ($thumb) {
            $pattern = '@upload/([a-z\-_0-9]*)\/([0-9]*)\/(.*\.(jpg|png|gif|jpeg|svg|JPG|PNG|GIF|JPEG|SVG))$@';
            $path = preg_replace($pattern, 'upload/$1/$2/thumb/$3', $path);
        }        
        
        return $path;
        
    }
    
    return get_template_directory_uri() . '/images/no-image.png';
    
}

/**
* passar para $cidade nome da cidade
* passar para $uf a unidade federativa
* a partir do $txt
*/
function cloudimo_desacoplar_cidade_uf(&$cidade, &$uf, $txt)
{
	if ($txt) {
		$xp = explode('-',$txt);
		$cidade = isset($xp[0])?$xp[0]:$cidade;
		$uf = isset($xp[1])?$xp[1]:'';
	}
}


/**
 * 
 */
 function cloudimo_get_ipaddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}