<?php

function cloudimo_buscar_lancamentos(&$result, $post)
{
    
    $args = array(
        'post_type'         => 'lancamento',
        'orderby'           => 'meta_value',
        'order'             => 'DESC',
        'posts_per_page'    => 9999999
    );
    
    
    $procurar = isset($post['procurar'])? $post['procurar']: '';
    
    
    if ($procurar) {
        $args['meta_query'] = array(
            'relation' => 'OR',
            array(
                'key'     => 'bairro',
                'value'   => $procurar,
                'compare' => 'LIKE',
            ),
            array(
                'key'     => 'cidade',
                'value'   => $procurar,
                'compare' => 'LIKE',
            ),
            array(
                'key'     => 'descricao',
                'value'   => $procurar,
                'compare' => 'LIKE',
            ),
        );
    }
    
    // The Query
    $lancamento = new WP_Query( $args );
    

    
    // Preparando lançamentos   
    while ($lancamento->have_posts()) {
        
        $lancamento->the_post();

        $titulo        = get_the_title();
        $post_id       = get_the_id();
        $thumbnail     = get_the_post_thumbnail_url( $post_id, 'medium' );
        $permalink     = get_permalink();
        $resumo        = get_field('resumo');
        $bairro        = get_field('bairro');
        $cidade        = get_field('cidade');

        $imov = new \ArrayObject();
        $imov->ID = 0;
        $imov->Tarja = '';
        $imov->CategoriaImovel = 'Lançamento';
        $imov->PrecoVenda = '';
        $imov->PrecoLocacao = '';
        $imov->Fotos = new \ArrayObject();
        $imov->Fotos->Foto = new \ArrayObject();
        $imov->Fotos->Foto->URLArquivo = $thumbnail;
        $imov->CodigoImovel = '';
        $imov->TipoImovel = $titulo;
        $imov->Bairro = $bairro;
        $imov->Cidade = $cidade;
        $imov->Uf = '';
        $imov->QtdSalas = 0;
        $imov->QtdSuites = 0;
        $imov->QtdDormitorios = 0;
        $imov->QtdBanheiros = 0;
        $imov->QtdVagasGaragem = 0;
        $imov->IDUsuario = 1;
        $imov->DataResumo = '';

        $imov->permalink = $permalink;
        $imov->resumo = $resumo;
        
        
        //$result_completo[] = $imov;
        
        $result->Imoveis->Imovel[] = $imov;
        $result->Imoveis->Total++;
            
    }
    // Fim preparando lançamentos
    
}

